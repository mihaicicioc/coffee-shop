# coffee-shop

Server app that does the following:
- load a csv containing pois (points of interest), in this case they are coffee shops (name address, latitude, longitude).
- presents an api that can do the following:
  - CRUD operations on pois
  - given a pair of (lat, lon), it will search the nearest poi.

Install with: `$npm install`
Run server with: `$npm start`
Run tests with: `$npm test`

The api is the following:
- GET `/api/poi/:id`
- POST `/api/poi` , form params: name, address, lat, lon
- PATCH `/api/poi/:id` , form params: name, address, lat, lon
- DELETE `/api/poi/:id`
- GET `/api/findnearest?address=...`

You can test the api using curl:
~~~sh
curl -X GET \
  http://localhost:3000/api/poi/17 \
  -H 'Cache-Control: no-cache'
~~~
~~~sh
curl -X POST \
  http://localhost:3000/api/poi \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'name=Cafe%20Escobar&address=600%20Vallejo%20St&lat=37.798682710638104&lon=-122.40722444643662'
~~~
~~~sh
url -X PATCH \
  http://localhost:3000/api/poi/49 \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d name=Cafe%20Escobar%20Pablo
~~~
~~~sh
curl -X DELETE \
  http://localhost:3000/api/poi/49 \
  -H 'Cache-Control: no-cache'
~~~
~~~sh
curl -X GET \
  'http://localhost:3000/api/findnearest?address=535%20Mission%20St.,%20San%20Francisco,%20CA' \
  -H 'Cache-Control: no-cache'
~~~
~~~sh
curl -X GET \
  'http://localhost:3000/api/findnearest?address=252%20Guerrero%20St,%20San%20Francisco,%20CA%2094103,%20USA' \
  -H 'Cache-Control: no-cache'
~~~
 