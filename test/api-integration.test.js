const chai = require('chai');
const chaiHttp = require('chai-http');

const { app } = require('../server');
const CoffeeShopModel = require('../app/model/db/CoffeeShop');

// eslint-disable-next-line no-unused-vars
const should = chai.should();

chai.use(chaiHttp);

describe('Test poi api endpoints', () => {
  before('remove all existing documents, to start fresh', done => {
    CoffeeShopModel.remove({}).then(() => done());
  });

  it('should get an existing poi', done => {
    chai
      .request(app)
      .get('/api/poi/13')
      .end((error, res) => {
        if (error) {
          done(error);
        } else {
          res.should.have.status(200);
          res.body.status.should.equal('success');
          res.body.data.id.should.equal('13');
          res.body.data.name.should.equal('Flywheel Coffee');
          res.body.data.address.should.equal('672 Stanyan St');
          res.body.data.lat.should.equal('37.76957397933175');
          res.body.data.lon.should.equal('-122.45336134825135');
          done();
        }
      });
  });

  it('should get not found for a non-existing poi', done => {
    chai
      .request(app)
      .get('/api/poi/113')
      .end((error, res) => {
        if (error) {
          done(error);
        } else {
          res.should.have.status(404);
          res.body.status.should.equal('not_found');
          done();
        }
      });
  });

  it('should add a new poi', done => {
    chai
      .request(app)
      .post('/api/poi')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send({
        name: 'Cafe Escobar',
        address: '600 Vallejo St',
        lat: 37.76957397933175,
        lon: -122.45336134825135
      })
      .end((error, res) => {
        if (error) {
          done(error);
        } else {
          res.should.have.status(200);
          res.body.status.should.equal('success');
          res.body.data.id.should.equal('50');
          done();
        }
      });
  });

  it('should update an existing poi', done => {
    chai
      .request(app)
      .patch('/api/poi/50')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send({
        name: 'Cafe Escobar Pablo'
      })
      .end((error, res) => {
        if (error) {
          done(error);
        } else {
          res.should.have.status(200);
          res.body.status.should.equal('success');
          res.body.data.id.should.equal('50');
          res.body.data.name.should.equal('Cafe Escobar Pablo');
          res.body.data.address.should.equal('600 Vallejo St');
          res.body.data.lat.should.equal('37.76957397933175');
          res.body.data.lon.should.equal('-122.45336134825135');
          done();
        }
      });
  });

  it('should get not found if updated poi does not exist', done => {
    chai
      .request(app)
      .patch('/api/poi/505')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send({
        name: 'Cafe Escobar Pablo'
      })
      .end((error, res) => {
        if (error) {
          done(error);
        } else {
          res.should.have.status(404);
          res.body.status.should.equal('not_found');
          done();
        }
      });
  });

  it('should delete an existing poi', done => {
    chai
      .request(app)
      .delete('/api/poi/50')
      .end((error, res) => {
        if (error) {
          done(error);
        } else {
          res.should.have.status(200);
          res.body.data[0].id.should.equal('50');
          res.body.data[0].name.should.equal('Cafe Escobar Pablo');
          res.body.data[0].address.should.equal('600 Vallejo St');
          res.body.data[0].lat.should.equal('37.76957397933175');
          res.body.data[0].lon.should.equal('-122.45336134825135');
          done();
        }
      });
  });

  it('should get not found if deleted poi does not exist', done => {
    chai
      .request(app)
      .delete('/api/poi/505')
      .end((error, res) => {
        if (error) {
          done(error);
        } else {
          res.should.have.status(404);
          res.body.status.should.equal('not_found');
          done();
        }
      });
  });
});

describe('Test find nearest api endpoints', () => {
  it('should get the nearest poi 1', done => {
    chai
      .request(app)
      .get('/api/findnearest')
      .query({ address: '535 Mission St., San Francisco, CA' })
      .end((error, res) => {
        if (error) {
          done(error);
        } else {
          res.should.have.status(200);
          res.body.status.should.equal('success');
          res.body.data.id.should.equal('17');
          res.body.data.name.should.equal('Red Door Coffee');
          res.body.data.address.should.equal('111 Minna St');
          done();
        }
      });
  });

  it('should get the nearest poi 2', done => {
    chai
      .request(app)
      .get('/api/findnearest')
      .query({ address: '252 Guerrero St, San Francisco, CA 94103, USA' })
      .end((error, res) => {
        if (error) {
          done(error);
        } else {
          res.should.have.status(200);
          res.body.status.should.equal('success');
          res.body.data.id.should.equal('29');
          res.body.data.name.should.equal('Four Barrel Coffee');
          res.body.data.address.should.equal('375 Valencia St');
          done();
        }
      });
  });
});
