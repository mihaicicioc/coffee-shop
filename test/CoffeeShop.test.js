const chai = require('chai');

const CoffeeShop = require('../app/model/CoffeeShop');

const coffeeShop = new CoffeeShop({
  name: 'Red Door Coffee',
  address: '111 Minna St',
  lat: 37.78746242830388,
  lon: -122.39933341741562
});

// eslint-disable-next-line no-unused-vars
const should = chai.should();

describe('Compute distance between 2 geo points', () => {
  it('should compute distance in km based on the Haversine formula', () => {
    const expectedDistInKm = coffeeShop.getDistanceFromThisToLatLonInKm(
      37.76098725908006,
      -122.43446774623341
    );
    expectedDistInKm.should.be.closeTo(4.26, 0.01);
  });
});
