require('./conf/config');

const csvtojson = require('csvtojson');
const fs = require('fs');

const { info, warn, error } = require('./conf/winston');
const app = require('./conf/express').app();
// eslint-disable-next-line no-unused-vars
const { mongoose } = require('./conf/mongoose');
const { coffeeShopDatasource } = require('./app/model/CoffeeShopDatasource');
const CoffeeShopModel = require('./app/model/db/CoffeeShop');

const csvFile = './data/locations.csv';

initCoffeeShopDatasourceFromCsv();

app.listen(process.env.PORT, () => {
  info(`Server is up on port ${process.env.PORT}`);
});

module.exports = {
  app
};

// functions

function initCoffeeShopDatasourceFromCsv() {
  if (csvFileExists()) {
    csvtojson({
      noheader: true,
      headers: ['cid', 'name', 'address', 'lat', 'lon']
    })
      .fromFile(csvFile)
      .subscribe(
        // eslint-disable-next-line prettier/prettier
        json => {
          coffeeShopDatasource.add(json);
          new CoffeeShopModel(json)
            .save()
            .then(() => {})
            .catch(err => {
              error(`error while saving to db: ${err}`);
            });
        },
        err => {
          error(`error while loading csv file ${err}`);
        },
        () => {
          info(`finished loading csv file`);
        }
      );
  }
}

function csvFileExists() {
  try {
    return fs.statSync(csvFile).isFile();
  } catch (err) {
    warn(`initial csv file does not exist: ${csvFile}`);
  }
}
