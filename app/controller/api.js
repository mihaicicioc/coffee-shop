const request = require('request');

const { error } = require('../../conf/winston');
const { geocodeServerUrl } = require('../../conf/config');
const { coffeeShopDatasource } = require('../model/CoffeeShopDatasource');

module.exports.getPoi = (req, res) => {
  const item = coffeeShopDatasource.getById(req.params.poi_id);
  if (item) {
    res.status(200).json({
      status: 'success',
      data: item
    });
  } else {
    res.status(404).json({
      status: 'not_found',
      message: `poi with id ${req.params.poi_id} not found`
    });
  }
};

module.exports.addPoi = (req, res) => {
  const id = coffeeShopDatasource.add({
    name: req.body.name,
    address: req.body.address,
    lat: req.body.lat,
    lon: req.body.lon
  });
  res.status(200).json({
    status: 'success',
    data: { id }
  });
};

module.exports.updatePoi = (req, res) => {
  const item = coffeeShopDatasource.updateById(req.params.poi_id, {
    name: req.body.name,
    address: req.body.address,
    lat: req.body.lat,
    lon: req.body.lon
  });
  if (item) {
    res.status(200).json({
      status: 'success',
      data: item
    });
  } else {
    res.status(404).json({
      status: 'not_found',
      message: `poi with id ${req.params.poi_id} not found`
    });
  }
};

module.exports.deletePoi = (req, res) => {
  const item = coffeeShopDatasource.deleteById(req.params.poi_id);
  if (item) {
    res.status(200).json({
      status: 'success',
      data: item
    });
  } else {
    res.status(404).json({
      status: 'not_found',
      message: `poi with id ${req.params.poi_id} not found, no need to delete`
    });
  }
};

module.exports.findNearest = (req, res) => {
  request(
    `${geocodeServerUrl}?q=${req.query.address}&format=json`,
    {
      json: true,
      headers: {
        'User-Agent': 'request'
      }
    },
    (err, resGeo, body) => {
      if (err) {
        error(`geocode error: ${err}`);
        res.status(503).json({
          status: 'service_unavailable',
          message: `geocode server error`
        });
      }

      if (body[0] !== undefined && body[0].lat !== undefined && body[0].lon !== undefined) {
        const { lat: userLat, lon: userLon } = body[0];

        res.status(200).json({
          status: 'success',
          data: coffeeShopDatasource.findClosestTo(Number(userLat), Number(userLon))
        });
      } else {
        res.status(404).json({
          status: 'not_found',
          message: `found no poi for address ${req.query.address}`
        });
      }
    }
  );
};
