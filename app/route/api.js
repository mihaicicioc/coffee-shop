const router = require('express').Router();

const apiController = require('../controller/api');

router.get('/', (req, res) => {
  res.status(200).json({
    status: 'success',
    message: 'api is ok'
  });
});

// eslint-disable-next-line prettier/prettier
router.route('/poi/:poi_id')
  .get(apiController.getPoi)
  .patch(apiController.updatePoi)
  .delete(apiController.deletePoi);

// eslint-disable-next-line prettier/prettier
router.route('/poi')
  .post(apiController.addPoi);

// eslint-disable-next-line prettier/prettier
router.route('/findnearest')
  .get(apiController.findNearest);

module.exports = router;
