const router = require('express').Router();

router.get('/', (req, res) => {
  res.status(200).send('request on /');
});

module.exports = router;
