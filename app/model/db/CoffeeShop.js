const mongoose = require('mongoose');

const { Schema } = mongoose;

const schema = new Schema({
  name: { type: String, trim: true, required: true },
  address: { type: String, trim: true, required: true },
  lat: { type: String, trim: true, required: true },
  lon: { type: String, trim: true, required: true }
});

schema.virtual('fullName').get(function fullName() {
  return `${this.name} - ${this.address}`;
});

const CoffeeShop = mongoose.model('CoffeeShop', schema);

module.exports = CoffeeShop;
