const CoffeeShop = require('./CoffeeShop');

/**
 * singleton data source
 */
class CoffeeShopDatasource {
  constructor() {
    this.items = [];
  }

  add(coffeeShop) {
    const cs = new CoffeeShop(coffeeShop);
    this.items.push(cs);
    return cs.id;
  }

  getById(id) {
    const index = findIndexById(this.items, id);
    if (index) {
      return this.items[index];
    }
    return undefined;
  }

  updateById(id, newValues) {
    const index = findIndexById(this.items, id);
    if (index) {
      this.patchCoffeeSHop(newValues, index);
      return this.items[index];
    }
    return undefined;
  }

  patchCoffeeSHop(newValuesCoffeeShop, index) {
    if (newValuesCoffeeShop.name !== undefined) {
      this.items[index].name = newValuesCoffeeShop.name;
    }
    if (newValuesCoffeeShop.address !== undefined) {
      this.items[index].address = newValuesCoffeeShop.address;
    }
    if (newValuesCoffeeShop.lat !== undefined) {
      this.items[index].lat = newValuesCoffeeShop.lat;
    }
    if (newValuesCoffeeShop.lon !== undefined) {
      this.items[index].lon = newValuesCoffeeShop.lon;
    }
  }

  deleteById(id) {
    const index = findIndexById(this.items, id);
    if (index) {
      return this.items.splice(index, 1);
    }
    return undefined;
  }

  findClosestTo(lat, lon) {
    let minId = this.items[0].id;
    let minDist = this.items[0].getDistanceFromThisToLatLonInKm(lat, lon);
    for (let i = 1; i < this.items.length; i += 1) {
      const dist = this.items[i].getDistanceFromThisToLatLonInKm(lat, lon);
      if (dist < minDist) {
        minDist = dist;
        minId = this.items[i].id;
      }
    }
    return this.getById(minId);
  }
}

function findIndexById(arr, id) {
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i].id === id) {
      return i;
    }
  }
  return undefined;
}

const instance = new CoffeeShopDatasource();
Object.freeze(instance);

module.exports.coffeeShopDatasource = instance;
