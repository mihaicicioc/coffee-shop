module.exports = class CoffeeShop {
  constructor({ name, address, lat, lon }) {
    this.id = String(CoffeeShop.counter);
    this.name = name;
    this.address = address;
    this.lat = lat;
    this.lon = lon;
  }

  /**
   * uses the Haversine formula,
   * https://en.wikipedia.org/wiki/Haversine_formula
   */
  getDistanceFromThisToLatLonInKm(lat, lon) {
    const R = 6371; // Radius of the earth in km
    const dLat = deg2rad(lat - this.lat);
    const dLon = deg2rad(lon - this.lon);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(deg2rad(this.lat)) *
        Math.cos(deg2rad(lat)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c; // Distance in km
    return d;
  }

  static get counter() {
    CoffeeShop.ucounter = (CoffeeShop.ucounter || 0) + 1;
    return CoffeeShop.ucounter;
  }
};

function deg2rad(deg) {
  return deg * (Math.PI / 180);
}
