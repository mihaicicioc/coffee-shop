const { createLogger, format, transports } = require('winston');
const fs = require('fs');

const { combine, timestamp, printf } = format;

const myFormat = printf(info => `${info.timestamp} [${info.level.toUpperCase()}] ${info.message}`);

const logDir = 'log';

if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

const logger = createLogger({
  format: combine(timestamp(), myFormat),
  transports: [
    new transports.File({ filename: `./${logDir}/error.log`, level: 'error' }),
    new transports.File({ filename: `./${logDir}/app.log` })
  ]
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(
    new transports.Console({
      format: format.combine(timestamp(), myFormat)
    })
  );
}

module.exports = logger;
