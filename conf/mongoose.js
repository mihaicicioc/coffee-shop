const mongoose = require('mongoose');

const { error } = require('./winston');

mongoose.Promise = global.Promise;
mongoose
  .connect(
    process.env.MONGODB_URI,
    {
      autoIndex: false,
      useNewUrlParser: true
    }
  )
  .catch(err => {
    error(`error while connecting to db: ${err}`);
  });

module.exports = {
  mongoose
};
