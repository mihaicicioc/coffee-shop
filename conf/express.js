const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const routerApi = require('../app/route/api');
const routerIndex = require('../app/route/index');
const router404 = require('../app/route/r404');

module.exports.app = () => {
  const app = express();

  app.use(morgan('dev'));

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  app.use('/api', routerApi);
  app.use('/', routerIndex);
  app.use('/', router404);

  return app;
};
