process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if (process.env.NODE_ENV === 'development') {
  process.env.PORT = 3000;
  process.env.MONGODB_URI = 'mongodb://localhost:27017/coffee-shop';
} else if (process.env.NODE_ENV === 'test') {
  process.env.PORT = 3000;
  process.env.MONGODB_URI = 'mongodb://localhost:27017/coffee-shop-test';
} else if (process.env.NODE_ENV === 'production') {
  process.env.PORT = process.env.PORT || 443;
  process.env.MONGODB_URI = 'mongodb://localhost:27017/coffee-shop';
}

const config = {
  geocodeServerUrl: 'https://nominatim.openstreetmap.org/search'
};

module.exports = config;
